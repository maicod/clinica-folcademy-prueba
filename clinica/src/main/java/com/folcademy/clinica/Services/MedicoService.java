package com.folcademy.clinica.Services;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Mappers.MedicoMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import com.folcademy.clinica.exceptions.BadRequestException;
import com.folcademy.clinica.exceptions.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service("medicoService")
public class MedicoService {
    private final MedicoRepository medicoRepository;
    private final MedicoMapper medicoMapper;

    public MedicoService(MedicoRepository medicoRepository, MedicoMapper medicoMapper) {
        this.medicoRepository = medicoRepository;
        this.medicoMapper = medicoMapper;
    }

    public List<MedicoDto> listarTodos() {
        return medicoRepository.findAll().stream().map(medicoMapper::entityToDto).collect(Collectors.toList());
    }

    public MedicoDto listarUno(Integer id) {
        return medicoRepository.findById(id).map(medicoMapper::entityToDto).orElse(null);
    }

    public MedicoDto agregar(MedicoDto entity) {
        entity.setId(null);
        if(entity.getConsulta()<0)
            throw new BadRequestException("La consulta no puede ser menor a 0");
        return medicoMapper.entityToDto(medicoRepository.save(medicoMapper.dtoToEntity(entity)));
    }

    public MedicoEnteroDto editar(Integer idMedico, MedicoEnteroDto dto) {
        if (!medicoRepository.existsById(idMedico))
           throw new NotFoundException("no existe");
//            return null;
        dto.setId(idMedico);

        return
                medicoMapper.entityToEnteroDto(
                        medicoRepository.save(
                                medicoMapper.enteroDtoToEntity(
                                        dto
                                )
                        )
                );
    }

    public boolean editarConsulta(Integer idMedico, Integer consulta) {
        if (medicoRepository.existsById(idMedico)) {
            Medico entity = medicoRepository.findById(idMedico).orElse(new Medico());
            entity.setConsulta(consulta);
            medicoRepository.save(entity);
            return true;
        }
        return false;
    }

    public boolean eliminar(Integer id) {
        if (!medicoRepository.existsById(id))
            return false;
        medicoRepository.deleteById(id);
        return true;
    }
}
